This module allows to set specific target for any webform (provided with Webform
module).

On enabling module new field Target appears at webform settings form, where one
can select desired target (_self, _blank, _parent, _top). For now setting frame
name as target is not implemented: it will be added once somebody request this
option.
