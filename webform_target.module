<?php

/**
 * @file
 */

/**
 * Implements hook_form_alter().
 */
function webform_target_form_webform_configure_form_alter(&$form, $form_state, $form_id) {
  if (isset($form['nid'])) {
    $default = webform_target_webform_target($form['nid']['#value']);
  }
  else {
    $default = '_self';
  }
  $form['submission']['target'] = array(
    '#type' => 'select',
    '#title' => t('Target'),
    '#options' => array(
      '_blank' => t('New window or tab (_blank)'),
      '_self' => t('Same frame (_self)'),
      '_parent' => t('Parent frame (_parent)'),
      '_top' => t('Full body of the window (_top)'),
    ),
    '#default_value' => $default,
    '#description' => t('Select where should the form submission page open: same window, new window, etc.'),
  );
  $form['#submit'][] = 'webform_target_form_webform_configure_form_submit';
}

/**
 * Submission callback for webform configure form.
 */
function webform_target_form_webform_configure_form_submit($form, $form_state) {
  db_merge('webform_target')
  ->key(array(
    'nid' => $form_state['values']['nid'],
  ))
  ->fields(array(
    'target' => $form_state['values']['target'],
  ))
  ->execute();
}

/**
 * Returns target for specific webform.
 */
function webform_target_webform_target($nid) {
  $record = db_query('SELECT target FROM {webform_target} WHERE nid = :nid', array(':nid' => $nid))->fetchObject();
  if ($record) {
    return $record->target;
  }
  else {
    return '_self';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function webform_target_form_alter(&$form, $form_state, $form_id) {
  if (strpos($form_id, 'webform_client_form') === 0) {
    $target = webform_target_webform_target($form['#node']->nid);
    if (!isset($form['#attributes'])) {
      $form['#attributes'] = array();
    }
    $form['#attributes']['target'] = $target;
  }
}
